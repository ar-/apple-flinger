/*******************************************************************************
 * Copyright (C) 2025 Andreas Redmer <ar-appleflinger@abga.be>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.gitlab.ardash.appleflinger.actors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Scaling;
import com.gitlab.ardash.appleflinger.AdvancedStage;
import com.gitlab.ardash.appleflinger.global.GameManager;

public abstract class ImageContainerGroup extends Group implements HasCenter{

	private Image image;
	protected GameManager gm;
	
	public ImageContainerGroup() {
		super();
		gm = GameManager.getInstance();
		image = new Image();
		this.addActor(image);
	}
	
	@Override
	public void setSize(float width, float height) {
		super.setSize(width, height);
		image.setSize(width, height);
	}
	
	protected Drawable getDrawable () {
		return image.getDrawable();
	}
	
	protected void setDrawable (Drawable d) {
		image.setDrawable(d);
	}
	
	protected void setScaling (Scaling s) {
		image.setScaling(s);
	}
	
	protected void setAlign (int a) {
		image.setAlign(a);
	}
	
	protected float getLocalCenterX() {
		return getWidth()/2f;
	}
	protected float getLocalCenterY() {
		return getHeight()/2f;
	}
	
	@Override
	public float getCenterX()
	{
		return getX()+getLocalCenterX();
	}
	
	@Override
	public float getCenterY()
	{
		return getY()+getLocalCenterY();
	}
	
	@Override
	public Vector2 getCenter()
	{
		return P.getVector2(getCenterX(),getCenterY());
	}
	
	public AdvancedStage getAdv() {
		return (AdvancedStage) getStage();
	}

}
