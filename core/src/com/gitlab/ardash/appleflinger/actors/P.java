/*******************************************************************************
 * Copyright (C) 2018-2025 Andreas Redmer <ar-appleflinger@abga.be>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.gitlab.ardash.appleflinger.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pools;

/**
 * A short convenience thing for pools. Stolen from OpenDefence.
 * @author Andreas Redmer
 */
public class P {
	
	static {
//		Pools.get(Line.class, 1000);
//		Pools.get(SmokeTrail.class, 8000);
	}
	
	public static void free(Object... o) {
		for (Object object : o) {
			Pools.free(object);
		}
	}

	public static Vector2 getVector2()
	{
		final Vector2 o = Pools.get(Vector2.class,200).obtain();
		o.set(0,0);
		return o;
	}

	public static Vector2 getVector2(float x, float y) {
		return getVector2().set(x, y);
	}

	public static Vector2 getVector2(Vector2 v) {
		return getVector2().set(v.x, v.y);
	}

	public static Color getColor() {
		return Pools.get(Color.class).obtain().set(0,0,0,0);
	}
	public static Color getColor(Color c) {
		return Pools.get(Color.class).obtain().set(c);
	}

}
