/*******************************************************************************
 * Copyright (C) 2025 Andreas Redmer <ar-appleflinger@abga.be>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.gitlab.ardash.appleflinger.actors;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.gitlab.ardash.appleflinger.GameWorld;
import com.gitlab.ardash.appleflinger.actions.AimTo;
import com.gitlab.ardash.appleflinger.global.Assets;
import com.gitlab.ardash.appleflinger.global.GameState;
import com.gitlab.ardash.appleflinger.global.MaterialConfig;

public abstract class EyeMovingActor extends GeneralTargetActor {

	protected enum EyeState 
		{
			LOOK_AT_RANDOM_POINT // default eye movement, move to random angle
			, LOOK_AT_OTHER_TARGET
			, BLINK // closed but very short
			, CLOSED // must be the last one (the ones after this one, are excluded from random selection)
			, LOOK_AT_BOTTOM
			, LOOK_AT_PROJECTILE
		}

	/** private lifetime */
	private float privateTime = 0;
	private float lastAnimTimestamp = 0;
	private static final float BLINK_CLOSED_TIME = 0.1f;
	private static final float MAX_TIME_TO_NEXT_ANIM = 3f;
	private float timeToNextEyeAnim = MathUtils.random(1f, MAX_TIME_TO_NEXT_ANIM);
	protected EyeState currentEyeState = EyeState.LOOK_AT_RANDOM_POINT;
	protected TextureRegionDrawable eyesClosedDrawable;
	protected Group eyeHolder;
	protected HingeGroup leftEyeHinge;
	protected HingeGroup rightEyeHinge;
	protected Image leftEye;
	protected Image rightEye;

	public EyeMovingActor(GameWorld world, MaterialConfig mc, float x, float y, float diameter, BodyType bodyType) {
		super(world, mc, x, y, diameter, bodyType);
        TextureRegionDrawable eye = new TextureRegionDrawable(Assets.SpriteAsset.EYE.get());
        // add all to each other
        eyeHolder = new Group();
        eyeHolder.setPosition(getLocalCenterX(), getLocalCenterY());
        leftEyeHinge = new HingeGroup();
        rightEyeHinge = new HingeGroup();
        leftEye = new Image(eye);
        rightEye = new Image(eye);
        leftEyeHinge.addActor(leftEye);
        rightEyeHinge.addActor(rightEye);
        eyeHolder.addActor(leftEyeHinge);
        eyeHolder.addActor(rightEyeHinge);
        this.addActor(eyeHolder);
        
        // move them around
        leftEye.setSize(0.0411f, 0.0411f);
        rightEye.setSize(0.0411f, 0.0411f);
        leftEye.setOrigin(Align.center);
        rightEye.setOrigin(Align.center);
        leftEye.moveBy(-0.01f, -0.01f); // move the eye upward from the hinge, so the actor initially looks up
        rightEye.moveBy(-0.01f, -0.01f); // move the eye upward from the hinge, so the actor initially looks up
        leftEyeHinge.moveBy(-0.078f, 0.101f); /// moves left eye to the left and slightly up, so the origin is in the center of the empty eyeball
        rightEyeHinge.moveBy(0.078f, 0.101f); /// moves right eye to the right and slightly up, so the origin is in the center of the empty eyeball

        // initial eye rotation
        final float initAngle = MathUtils.random(360f);
        leftEyeHinge.addAction(Actions.rotateBy(initAngle, 1f));
        rightEyeHinge.addAction(Actions.rotateBy(initAngle, 1f));
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		setAnimationStatus();
//		rightEyeHinge.setRotation(rightEyeHinge.getRotation()+1f);

		switch (currentEyeState) {
		case CLOSED:
		case BLINK:
			eyeHolder.setVisible(false);
			break;
		//case DEFAULT:
		default:
			eyeHolder.setVisible(true);
			break;
		}

	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		if (eyesClosedDrawable != null) {
			// choose a foreground (eyes etc.) and draw it
			if (currentEyeState == EyeState.CLOSED || currentEyeState == EyeState.BLINK) {
				// set the normal actor drawable for the background, and draw it
				final Drawable originalDrawable = getDrawable();
				setDrawable(eyesClosedDrawable);
				super.draw(batch, parentAlpha);

				// set back to normal for nest draw
				setDrawable(originalDrawable);
			}
		}
	}

	private EyeState pickRandomEyeState() {
			final EyeState ret;
			//1 in 3 chance for a blink
			final int blinkChance = MathUtils.random(0, 2);
			if (blinkChance==0)
			{
				ret = EyeState.BLINK;
			}
			else
			{
				// same change for all states including blink
				final int max = EyeState.CLOSED.ordinal();
				final int rand = MathUtils.roundPositive(MathUtils.randomTriangular(0, max, 0));
				ret = EyeState.values()[rand];
			}
			
			return ret;
		}

	protected void setAnimationStatus() {
		privateTime += Gdx.graphics.getDeltaTime();
		
		// if falling down: look down immediately (use velocity value in physics)
		final Vector2 linearVelocity = body.getLinearVelocity();
		if (linearVelocity.y < 0) {
			//System.out.println("falling");
			startLookingAt(EyeState.LOOK_AT_BOTTOM, 1000f);
		}
		// if dragging: look at projectile
		if (gm.getGameState() == GameState.DRAGGING) {
			// look to projectile slowly
			startLookingAt(EyeState.LOOK_AT_PROJECTILE, 40f);
			return;
		}
		// if apple is in air and if apple still there
		if (getAdv().getCurrentProjectile() != null && !getAdv().getCurrentProjectile().isToBeDestroyed()) {
			// check if very close and then blink eyes
			if (getDistanceTo(getAdv().getCurrentProjectile()) < 1f) {
				currentEyeState = EyeState.BLINK;
				timeToNextEyeAnim = MathUtils.random(BLINK_CLOSED_TIME, BLINK_CLOSED_TIME*2f);
			}
			// follow with eyes
			if (gm.getGameState() == GameState.WAIT_FOR_PHYSICS ) {
				// follow projectile quickly
				startLookingAt(EyeState.LOOK_AT_PROJECTILE, 1000f);
				return;
			}
		}
		// when lost: look down (sad)
		if (gm.getGameState() == GameState.GAME_OVER_SCREEN)
			startLookingAt(EyeState.LOOK_AT_BOTTOM, 100f);
		
		// is it time for the next animation (or animation step) ?
		if (lastAnimTimestamp+timeToNextEyeAnim <= privateTime)
		{
			lastAnimTimestamp = privateTime;
			
			//choose the next step
			switch (currentEyeState) {
			case CLOSED:
				currentEyeState = EyeState.LOOK_AT_OTHER_TARGET;
				break;
			default:
				currentEyeState = pickRandomEyeState();
				break;
			}
			
			// react to the new state
			switch (currentEyeState) {
			case LOOK_AT_RANDOM_POINT:
				startLookingAt(EyeState.LOOK_AT_RANDOM_POINT,200);
				break;
			case LOOK_AT_OTHER_TARGET:
				startLookingAt(EyeState.LOOK_AT_OTHER_TARGET,200);
				break;
			default:
				// this cannot happen
				break;
			}
	
			timeToNextEyeAnim = MathUtils.random(1f, MAX_TIME_TO_NEXT_ANIM);
			// eye blinking can be shorter
			if (currentEyeState == EyeState.BLINK)
				timeToNextEyeAnim = MathUtils.random(BLINK_CLOSED_TIME, BLINK_CLOSED_TIME*2f);
		}
		
	}

	private void startLookingAt(EyeState desiredEyeState, float speed) {
		final EyeMovingActor me = this;
		leftEyeHinge.clearActions();
		rightEyeHinge.clearActions();
		switch (desiredEyeState) {
		case LOOK_AT_RANDOM_POINT:
			// random points didn't look good, but random other target looks great
			HasCenter randomPoint = getRandomOtherActor();
			leftEyeHinge.addAction(new AimTo(randomPoint, speed));
			rightEyeHinge.addAction(new AimTo(randomPoint, speed));
			break;
		case LOOK_AT_PROJECTILE:
			ProjectileActor pa = getAdv().getCurrentProjectile();
			leftEyeHinge.addAction(new AimTo (pa,speed));
			rightEyeHinge.addAction(new AimTo (pa,speed));
			break;
		case LOOK_AT_OTHER_TARGET:
			GeneralTargetActor ta = getClosestActor();
			leftEyeHinge.addAction(new AimTo (ta,speed));
			rightEyeHinge.addAction(new AimTo (ta,speed));
			break;
		case LOOK_AT_BOTTOM:
			HasCenter bottomPoint = new HasCenterImpl() {
				@Override
				public Vector2 getCenter() {
					return P.getVector2(me.getCenterX(),0);
				}
			};
			leftEyeHinge.addAction(new AimTo(bottomPoint, speed));
			rightEyeHinge.addAction(new AimTo(bottomPoint, speed));
			break;
		default:
			break;
		}
		
	}

	private GeneralTargetActor getRandomOtherActor() {
	    // Get all Target Actors on the stage
	    final Set<Actor> allTargetActors = getAdv().getAllTargetActors();
	    if (allTargetActors.isEmpty())
	        return null;

	    // Remove myself from the set
	    allTargetActors.remove(this);
	    if (allTargetActors.isEmpty())
	        return null;

	    // Convert the set to a list to allow indexed access
	    List<Actor> actorList = new ArrayList<>(allTargetActors);

	    // Use libGDX's MathUtils to select a random index
	    int randomIndex = MathUtils.random(actorList.size() - 1);
	    return (GeneralTargetActor) actorList.get(randomIndex);
	}


	/** 
	 * @return The closest targetActor on the stage, that is not dead and not myself. (it can be an enemy though). null if there is noone else.
	 */
	private GeneralTargetActor getClosestActor() {
	    // Get all Target Actors on the stage
	    final Set<Actor> allTargetActors = getAdv().getAllTargetActors();
	    if (allTargetActors.isEmpty())
	        return null;

	    // Remove myself from the set
	    allTargetActors.remove(this);
	    if (allTargetActors.isEmpty())
	        return null;

	    GeneralTargetActor closest = null;
	    float minDistanceSquared = Float.MAX_VALUE;
	    final float thisX = getX();
	    final float thisY = getY();

	    // Iterate through remaining actors to find the closest
	    for (Actor actor : allTargetActors) {
	        // Assuming all actors are instances of GeneralTargetActor
	        GeneralTargetActor target = (GeneralTargetActor) actor;
	        float distanceSquared = getDistanceTo(target, thisX, thisY);

	        if (distanceSquared < minDistanceSquared) {
	            minDistanceSquared = distanceSquared;
	            closest = target;
	        }
	    }
	    return closest;
	}

}