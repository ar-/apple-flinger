/*******************************************************************************
 * Copyright (C) 2015-2025 Andreas Redmer <ar-appleflinger@abga.be>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.gitlab.ardash.appleflinger.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.gitlab.ardash.appleflinger.GameWorld;
import com.gitlab.ardash.appleflinger.global.Assets;
import com.gitlab.ardash.appleflinger.global.Assets.SoundGroupAsset;
import com.gitlab.ardash.appleflinger.global.MaterialConfig;
import com.gitlab.ardash.appleflinger.helpers.SoundPlayer;

public class DorkActor extends EyeMovingActor{
	
	public DorkActor(GameWorld world, MaterialConfig mc, float x, float y,
			float diameter, BodyType bodyType) {
		super(world, mc, x, y, diameter, bodyType);
		
        eyesClosedDrawable = new TextureRegionDrawable(Assets.SpriteAsset.EYES_CLOSED_DORK.get());

        // this actually sets the size of the image independently from the physic actor!!!
        scaleBy(0.25f);
        
        // configure the points-label
        damageLabelColor = Color.GREEN;
        
	}
	
	public DorkActor(GameWorld world, MaterialConfig mc, float x, float y,
			float diameter) {
		this(world, mc, x, y, diameter, BodyType.DynamicBody);
	}
	
	@Override
	public void playHitSound() {
		SoundPlayer.playSound(Assets.getRandomSound(SoundGroupAsset.DORK_HIT));
	}
	
	@Override
	public String toJavaString() {
		if (bodyType == BodyType.DynamicBody)
			return "group.addActor (new DorkActor(world, MaterialConfig."+mc+", "+(body.getPosition().x)+"f, "+(body.getPosition().y)+"f, 0.5f));";    
	
		return "group.addActor (new DorkActor(world, MaterialConfig."+mc+", "+(body.getPosition().x)+"f, "+(body.getPosition().y)+"f, 0.5f, BodyType."+getBodyType()+"));";     
	}
	
}
