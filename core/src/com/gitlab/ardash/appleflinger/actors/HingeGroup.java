/*******************************************************************************
 * Copyright (C) 2025 Andreas Redmer <ar-appleflinger@abga.be>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.gitlab.ardash.appleflinger.actors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;

/**
 * A group that can be rotated around its origin, but the children will counter-rotate.
 * Can be used to rotate the pupils inside the eye, but do not rotate the eye itself.
 * The eye has a reflection at the top right and it should remain in the top right. 
 * 
 * It implements HasCenter, so it can be used as AimTo target. That way, the Actors look into their eyes
 * and not towards their stomach. It makes a big difference.
 */
public class HingeGroup extends Group implements HasCenter{
	
	@Override
	public void rotateBy(float amountInDegrees) {
		super.rotateBy(amountInDegrees);
		getChild(0).rotateBy(-amountInDegrees);
	}
	
	@Override
	public void setRotation(float degrees) {
		super.setRotation(degrees);
		getChild(0).setRotation(-degrees);
	}

	protected float getLocalCenterX() {
		return getWidth()/2f;
	}
	protected float getLocalCenterY() {
		return getHeight()/2f;
	}
	
	@Override
	public float getCenterX()
	{
		return getX()+getLocalCenterX();
	}
	
	@Override
	public float getCenterY()
	{
		return getY()+getLocalCenterY();
	}
	
	@Override
	public Vector2 getCenter()
	{
		return P.getVector2(getCenterX(),getCenterY());
	}
}
