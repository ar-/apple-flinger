/*******************************************************************************
 * Copyright (C) 2017,2018,2025 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.gitlab.ardash.appleflinger.actions;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gitlab.ardash.appleflinger.actors.HasCenter;
import com.gitlab.ardash.appleflinger.actors.P;

/**
 * Stolen from OpenDefence.
 */
public class AimTo extends Action {
	
	/**
	 * The enemy to aim at.
	 */
	private HasCenter enemy;
	
	/**
	 * Rotational speed in degrees per second.
	 */
	private float rotationSpeed;
	
	/**
	 * remaining angle to Enemy, after aiming
	 */
	private float angleToEnemy=Float.MAX_VALUE;

	/**
	 * Rotation has has actually been applied to the actor after act().
	 * This is store here, so it can be used in class FlyTo.
	 */
	protected float rotationAppliedToActor;
	
	

	public AimTo(HasCenter enemy, float rotationSpeed) {
		super();
		this.enemy = enemy;
		this.rotationSpeed = rotationSpeed;
	}
	
	public void setEnemy(HasCenter enemy) {
		this.enemy = enemy;
	}

	public void setRotationSpeed(float rotationSpeed) {
		this.rotationSpeed = rotationSpeed;
	}
	@Override
	public boolean act(float delta) {
	    if (enemy == null || target == null)
	        return false;

	    // Get enemy position (in stage coordinates)
	    Vector2 enemyPos = enemy.getCenter();
	    // Optionally, aim at the mouse:
	    // enemyPos = target.getStage().screenToStageCoordinates(P.getVector2(Gdx.input.getX(), Gdx.input.getY()));
	    
	    // Get the actor's center in stage coordinates (using its origin)
	    Vector2 myPos = P.getVector2();
	    Vector2 originVector = P.getVector2().set(target.getOriginX(), target.getOriginY());
	    myPos.add(target.localToStageCoordinates(originVector));

	    // Calculate the desired angle (in stage/global space) toward the enemy
	    double targetAngle = Math.atan2(enemyPos.y - myPos.y, enemyPos.x - myPos.x) * (180 / Math.PI);
	    float targetAngleF = (float) targetAngle - 45; // subtract any additional offset if needed

	    // Compute the cumulative rotation of all parents (i.e. the parent's global rotation)
	    float parentRotation = 0;
	    Actor parent = target.getParent();
	    while (parent != null) {
	        parentRotation += parent.getRotation();
	        parent = parent.getParent();
	    }

	    // Calculate the actor's current global rotation:
	    float globalRotation = target.getRotation() + parentRotation;

	    // Compute the difference between desired global angle and current global angle
	    float angleDiff = ((targetAngleF - globalRotation + 180 + 360) % 360) - 180;

	    // Determine the maximum rotation allowed this tick
	    float maxDegreesForThisTick = rotationSpeed * delta;
	    float rotationStep = MathUtils.clamp(angleDiff, -maxDegreesForThisTick, maxDegreesForThisTick);

	    // Calculate new global rotation and convert it back to the actor's local rotation
	    float newGlobalRotation = globalRotation + rotationStep;
	    float newLocalRotation = newGlobalRotation - parentRotation;

	    target.setRotation(newLocalRotation);
	    angleToEnemy = angleDiff;

	    P.free(myPos);
	    P.free(originVector);
	    P.free(enemyPos);

	    return false;
	}

	public float getAngleToEnemy() {
		return angleToEnemy;
	}
	
	

}

