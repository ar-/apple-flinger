ملاحظة: لا يحتوي هذا التطبيق على ميزات مضادة أو ميزات غير مرغوب فيها. ما تراه أدناه هو خطأ في F-Droid.

1.4.11
  * إصلاحات الترجمة الطفيفة. بواسطة verdulo مرة أخرى.
  * نسخة SDK الهدف الأعلى لمتجر جوجل بلايستور الغبي
