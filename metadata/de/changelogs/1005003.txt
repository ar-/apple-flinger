1.5.3
  * Adaptives Symbol hinzugefügt (Android 8 und höher)
  * Hintergrundzoom in der Nähe des Apfels deaktiviert
  * Norwegische Sprache hinzugefügt (Dank an Allan auf Weblate)

1.5.2
  * Portugiesische Übersetzung hinzugefügt
  * Gewinner und die höchsten Punkte grün anzeigen
  * Hardware-Zurück-Taste aktiviert, um das Spiel zu verlassen oder zum vorherigen Bildschirm zurückzukehren
  * Verbesserte polnische und Esperanto-Übersetzungen (von Verdulo)
