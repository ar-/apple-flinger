1.5.5
  * wiki-n, leírva az új szintek létrehozása
  * hozzáadva: 12 további téli szint
  * Hozzáadott értékek százalékos arány megjelenítése
  * hozzáadta az időt a képernyőn

1.5.4
  * hozzáadott fordítók a játék kredit képernyőjéhez
  * holland nyelv hozzáadásával (hála a Heimennek)
  * hozzáadta a galíciai nyelvet (köszönhetően Ivánnakn)
  * javult a spanyol fordítás (köszönhetően Markelnekn)
  * átlátszó képpontokat távolított el a képernyőképekről

1.5.3
  * hozzáadott adaptív ikon (Android 8 és újabb)
  * tiltott háttérkép nagyítás az alma közelében
  * norvég nyelvvel kiegészítve (köszönhetően Allannak a weboldalon)
